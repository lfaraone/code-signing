import datetime

from sqlalchemy import (
    create_engine,
    Column,
    Integer,
    String,
    DateTime)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session



# TODO(lfaraone): use a real database
engine = create_engine('sqlite:///dev.sqlite', echo=True)

DBSession = sessionmaker(bind=engine)  # type: Session

Base = declarative_base()


class AuditLog(Base):
    __tablename__ = 'audit_log'

    id = Column(Integer, primary_key=True)
    presign_id = Column(Integer, nullable=True)
    ts = Column(DateTime, default=datetime.datetime.now)
    binpkg_name = Column(String(256))
    binpkg_version = Column(String(256))
    file_path = Column(String(4096))
    fhash = Column(String(64), nullable=True)
    shash = Column(String(64), nullable=True)


class PackageState(Base):
    __tablename__ = 'package_state'
    id = Column(Integer, primary_key=True)
    ts = Column(DateTime, default=datetime.datetime.now)
    template_package_name = Column(String)
    template_package_version = Column(String)
    state = Column(String(64), nullable=False)
    error_msg = Column(String)
    architecture = Column(String)



Base.metadata.create_all(engine)

# type: ignore
